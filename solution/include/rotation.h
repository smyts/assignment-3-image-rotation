//
// Created by smyts on 11.10.2022.
//

#ifndef UNTITLED_ROTATION_H
#define UNTITLED_ROTATION_H

struct image rotate(const struct image old_image);

#endif //UNTITLED_ROTATION_H
