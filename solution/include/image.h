//
// Created by smyts on 11.10.2022.
//

#ifndef UNTITLED_IMAGE_H
#define UNTITLED_IMAGE_H

#include <malloc.h>
#include <stdint.h>

struct __attribute__ ((packed)) pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};


struct  image {
    uint64_t width;
    uint64_t height;
    struct pixel *data;
};



struct image create_image(const uint64_t width, const uint64_t height);



#endif //UNTITLED_IMAGE_H
