//
// Created by smyts on 15.10.2022.
//

#include "../include/rotation.h"
#include "../include/image.h"

struct image rotate(const struct image old_image) {
    struct image new_img = create_image(old_image.height, old_image.width);
    for (uint64_t y = 0; y < new_img.height; ++y) {
        for (uint64_t x = 0; x < new_img.width; ++x) {
            new_img.data[y * new_img.width + x] = old_image.data[(old_image.height - 1 - x) * old_image.width + y];
        }
    }
    return new_img;
}



