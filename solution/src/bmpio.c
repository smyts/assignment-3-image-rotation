//
// Created by smyts on 12.10.2022.
//

#include "../include/bmpio.h"
#include <inttypes.h>

#define TYPE 19778
#define ZERO_VALUE 0
#define OFFBITS 54
#define SIZE 40
#define PLANES 1
#define BITCOUNT 24

inline static uint8_t padding_size(const uint64_t width) {
    return (4 - (sizeof(struct pixel) * width)) % 4;
}

inline static struct bmp_header create_bmp_header(struct image image) {

    return (struct bmp_header) {
            .bfType = TYPE,
            .bfileSize = sizeof(struct bmp_header) +
                         image.height * (sizeof(struct pixel) * image.width +
                                         padding_size(image.width)),
            .bfReserved = ZERO_VALUE,
            .bOffBits = OFFBITS,
            .biSize = SIZE,
            .biWidth = image.width,
            .biHeight = image.height,
            .biPlanes = PLANES,
            .biBitCount = BITCOUNT,
            .biCompression = ZERO_VALUE,
            .biSizeImage = image.height * (sizeof(struct pixel) * image.width +
                                           padding_size(image.width)),
            .biXPelsPerMeter = ZERO_VALUE,
            .biYPelsPerMeter = ZERO_VALUE,
            .biClrUsed = ZERO_VALUE,
            .biClrImportant = ZERO_VALUE
    };
}

enum read_status from_bmp(FILE* in, struct image* img ) {
    struct bmp_header header = {0};

    if (!fread(&header, sizeof(struct bmp_header), 1, in)) {
        return READ_INVALID_HEADER;
    }


    if (header.bfType != 0x4d42 && header.bfType != 0x4349 && header.bfType != 0x5450 ) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != 24 ) {
        return READ_INVALID_BITS;
    }

    *img = create_image(header.biWidth, header.biHeight);

    if (img->data == NULL) {
	return READ_ERROR;
    }

    for ( uint64_t i = 0; i < img->height; i++ ) {
        uint64_t count_read =  fread( img->data + img->width * i, sizeof(struct pixel), img->width, in);
        if ( count_read != img->width ) {
            return READ_ERROR;
        }
        if ( fseek( in, padding_size(img->width), SEEK_CUR ) ) {
            return READ_ERROR;
        }
    }

    return READ_OK;

}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = create_bmp_header(*img);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    for (uint64_t i = 0; i < img->height; i++) {
        if (!fwrite(img->data + i * img->width,sizeof(struct pixel) * img->width,1,out)) {
            return WRITE_ERROR;
        }

	if (fseek(out, padding_size(img->width), SEEK_CUR) == -1) {
	   return WRITE_ERROR;
	}
    }

    return WRITE_OK;


}
