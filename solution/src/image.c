//
// Created by smyts on 12.10.2022.
//

#include "../include/image.h"

struct image create_image(const uint64_t width, const uint64_t height) {
    struct pixel* data = malloc(sizeof(struct pixel) * width * height );
    return (struct image) {.width = width, .height = height, .data = data};
}
