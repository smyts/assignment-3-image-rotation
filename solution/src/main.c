#include "../include/bmpio.h"
#include "../include/rotation.h"

int main( int argc, char *argv[] )  {
    if (argc < 3) {
        printf("incorrect arguments");
        return -1;
    }

    FILE* file;
    file = fopen(argv[1], "r");

    if (!file) {
	return -1;
    }

    struct image image = {0};

    if (from_bmp(file, &image) != READ_OK) {
	printf("bmp load error");
	return -1;
    }

    if (fclose(file) == EOF) {
	return -1;
    }

    struct image new_image = rotate(image);

    if (new_image.data == NULL) {
	printf("rotation error");
	return -1;
    }

    FILE* new_file;

    new_file = fopen(argv[2], "w");

    if (!new_file) {
	return -1;
    }

    if (to_bmp(new_file, &new_image) != WRITE_OK) {
	printf("new bmp file load error");
	return -1;
    }

    free(image.data);
    free(new_image.data);

    if (fclose(new_file) == EOF) {
	return -1;
    }

    return 0;
}
